import java.util.Random;
public class RouletteWheel 
{
    //CREATION OF FIELDS
    private Random randomNum;
    //stores the number of last spin
    private int number;

    //CONSTRUCTOR 
    public RouletteWheel()
    {
        this.randomNum = new Random();
        this.number = 0;
    }

    //CREATION OF SPIN METHOD THAT UPDATES LAST SPINNED NUMBER
    public void spin()
    {
        this.number = randomNum.nextInt(37);
    }

    //CREATION OF GETVALUE METHOD THAT RETURNS THE VALUE OF FIELD NUMBER
    public int getValue()
    {
        return this.number;
    }
}
