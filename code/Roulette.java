import java.util.Scanner;
public class Roulette
{
    public static void main(String args[])
    {
        Scanner scan = new Scanner(System.in);
        //creating new wheel of game
        RouletteWheel wheel = new RouletteWheel();
        //user 
        int playersMoney = 1000;
        while(true)
        {
            System.out.println("Would you like to make a bet? y for Yes or n for no?");
            String answer = scan.next();
            if(answer.equals("n"))
            {
                System.out.println("Thank you for playing!");
                break;
            }
            System.out.println("What number do you want to bet on?");
            int betNum = scan.nextInt();
            while(betNum >= 37)
            {
                System.out.println("You cannot bet over 36");
                betNum = scan.nextInt();
            }
            System.out.println("What amount of money do you want to bet on number " + betNum);
            int betMoney = scan.nextInt();
            //test if bet is in budget
            while(betMoney > playersMoney)
            {
                System.out.print("You don't have enough money ");
                System.out.println("What amount do you want to bet?");
                betMoney = scan.nextInt();
            }
            //spin roulette
            wheel.spin();
            System.out.println("The result number is: " + wheel.getValue());
            System.out.println(win(betNum, wheel, betMoney, playersMoney));
        }
        scan.close();
    }

    public static String win(int betNum, RouletteWheel wheel, int betMoney, int playersMoney)
    {
        String win = "";
        if(betNum == wheel.getValue())
        {
            playersMoney = playersMoney + (betMoney * 35);
            win = "You have won this round here is your balance: " + playersMoney;
        }else{
            playersMoney = playersMoney - betMoney;
            win = "You have lost this round here is your balance: " + playersMoney;
        }
        return win;
    }

}